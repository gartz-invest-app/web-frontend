const HEADER = "Gartz Investment";

const NAVBAR_DATA = [
    { id: 1, url: "#features", label: "Features" },
    { id: 2, url: "#roadmap", label: "Roadmap" },
    { id: 3, url: "#products", label: "Products" },
    { id: 4, url: "#testimonials", label: "Testimonials" },
    { id: 5, url: "#contact", label: "Contact" }
];

const SIGNUP_BUTTON = "Sign Up"


const DATA = {
    HEADER,
    NAVBAR_DATA,
    SIGNUP_BUTTON,
};

export default DATA;